﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Accuweather
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }
        
        public async void Page_Loaded(object sender, RoutedEventArgs e)
        { 
            DailyForecastRootObject daily = await API.Daily("tbFOLXfZmAxAexEYOmXhcxnbZBDjQBSh", "vi-vn");
            for (var i = 0; i < daily.DailyForecasts.Count; i++)
            {
                daily.DailyForecasts[i].Day.Icon = String.Format("https://www.accuweather.com/images/weathericons/{0}.svg", daily.DailyForecasts[i].Day.Icon);
            }
            DailyForecasts.ItemsSource = daily.DailyForecasts;
            DailyForecastsTextBook.Text = "Daily Forecasts";

            HourlyRootObject hourly = await API.Hourly("tbFOLXfZmAxAexEYOmXhcxnbZBDjQBSh", "vi-vn");
     
        }
    }
}
