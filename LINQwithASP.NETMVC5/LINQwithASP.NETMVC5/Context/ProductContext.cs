﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LINQwithASP.NETMVC5.Context
{
    public class ProductContext : DbContext
    {
        public DbSet<Models.Product> Products { get; set; }
    }
}