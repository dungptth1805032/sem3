﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWP_Tutorial_02.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string CoverImage { get; set; }

    }

    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var book = new List<Book>();
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/1.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/2.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/3.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/4.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/5.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/6.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/7.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/8.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/9.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/10.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/11.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/12.png" });
            book.Add(new Book { BookId = 1, Title = "Valuze", Author = "Futurum", CoverImage = "Assets/Books/12.png" });
            return book;

        }
    }
}
