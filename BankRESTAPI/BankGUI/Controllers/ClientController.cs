﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BankGUI.Controllers
{
    public class ClientController : Controller
    {
        public ActionResult GetHistory()
        {
            ViewBag.history = null;
            return View();
        }

        [HttpPost]
        public ActionResult GetHistory(TransferReference.ClientGetTransferHistory collection)
        {
            var client = new WebClient();
            var dataString = JsonConvert.SerializeObject(collection);

            client.Headers.Add(HttpRequestHeader.ContentType, "application/json");

            var content = client.UploadString("http://localhost:60134/TransferService.svc/ClientGetTransferHistory/", dataString);
            
            var json_serializer = new JavaScriptSerializer();
            ViewBag.history = json_serializer.Deserialize<List<TransferReference.ClientGetTransferHistoryResult>>(content);
            return View();
        }
    }
}
