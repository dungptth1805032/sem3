﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceREST
{
    public class EmployeeNew : IEmployeeNew
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();
        public List<Employee> GetProductList() {
            try
            {
                return (from employee in data.Employees select employee).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool AddEmployee(Employee eml) {
            try
            {
                data.Employees.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateEmployee(Employee eml) {
            try
            {
                Employee employeeToModify =
                    (from employee in data.Employees where employee.empID == eml.empID select employee).Single();
                employeeToModify.age = eml.age;
                employeeToModify.firstName = eml.firstName;
                employeeToModify.lastName = eml.lastName;
                employeeToModify.address = eml.address;
                data.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteEmployee(int idE) {
            try
            {
                Employee employeeToDelete =
                        (from employee in data.Employees where employee.empID == idE select employee).Single();
                data.Employees.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
