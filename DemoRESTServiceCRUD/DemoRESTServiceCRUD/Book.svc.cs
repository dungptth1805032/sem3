﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DemoRESTServiceCRUD
{
    public class Book : IBook
    {
        static BookReponsitory reponsitory = new BookReponsitory();
        public List<BookModel> GetBookList()
        {
            return reponsitory.GetAllBook();
        }

        public BookModel GetBookById(string id)
        {
            return reponsitory.GetBookById(int.Parse(id));
        }

        public string AddBook(BookModel book)
        {
            BookModel newBook = reponsitory.AddNewBook(book);
            return "id=" + newBook.BookId;
        }

        public string UpdateBook(BookModel book, string id)
        {
            bool updateResult = reponsitory.UpdateBook(book);
            if (updateResult)
            {
                return "Success";
            }
            return "False";
        }

        public string DeleteBook(string id)
        {
            bool deleteResult = reponsitory.DeleteBook(int.Parse(id));
            if(deleteResult)
            {
                return "Success";
            }
            return "False";
        }
    }
}
