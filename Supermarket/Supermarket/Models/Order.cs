//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Supermarket.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            this.OrderDeatails = new HashSet<OrderDeatail>();
        }
    
        public int orderCode { get; set; }
        public int employeeCode { get; set; }
        public int customerCode { get; set; }
        public int statusCode { get; set; }
        public int paymentCode { get; set; }
        public Nullable<System.DateTime> orderDate { get; set; }
        public Nullable<System.DateTime> shippedDate { get; set; }
        public string comments { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual Employee Employee { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDeatail> OrderDeatails { get; set; }
        public virtual Status Status { get; set; }
        public virtual Payment Payment { get; set; }
    }
}
