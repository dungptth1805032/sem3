﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Supermarket.Models;

namespace Supermarket.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private SupermarketAppEntities db = new SupermarketAppEntities();

        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.Category).Include(p => p.Warehouse).Include(p => p.Slot);
            return View(products.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.categoryCode = new SelectList(db.Categories, "categoryCode", "categoryName");
            ViewBag.warehouseCode = new SelectList(db.Warehouses, "warehouseCode", "warehouseAddress");
            ViewBag.slotCode = new SelectList(db.Slots, "slotCode", "slotName");
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "productCode,productName,productPrice,productDescription,categoryCode,warehouseCode,productLnStock,slotCode,productAvailable,lastSold,createdDate")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.categoryCode = new SelectList(db.Categories, "categoryCode", "categoryName", product.categoryCode);
            ViewBag.warehouseCode = new SelectList(db.Warehouses, "warehouseCode", "warehouseAddress", product.warehouseCode);
            ViewBag.slotCode = new SelectList(db.Slots, "slotCode", "slotName", product.slotCode);
            return View(product);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.categoryCode = new SelectList(db.Categories, "categoryCode", "categoryName", product.categoryCode);
            ViewBag.warehouseCode = new SelectList(db.Warehouses, "warehouseCode", "warehouseAddress", product.warehouseCode);
            ViewBag.slotCode = new SelectList(db.Slots, "slotCode", "slotName", product.slotCode);
            return View(product);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "productCode,productName,productPrice,productDescription,categoryCode,warehouseCode,productLnStock,slotCode,productAvailable,lastSold,createdDate")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.categoryCode = new SelectList(db.Categories, "categoryCode", "categoryName", product.categoryCode);
            ViewBag.warehouseCode = new SelectList(db.Warehouses, "warehouseCode", "warehouseAddress", product.warehouseCode);
            ViewBag.slotCode = new SelectList(db.Slots, "slotCode", "slotName", product.slotCode);
            return View(product);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
