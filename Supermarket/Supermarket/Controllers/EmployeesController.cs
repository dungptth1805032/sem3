﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Supermarket.Models;

namespace Supermarket.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EmployeesController : Controller
    {
        private SupermarketAppEntities db = new SupermarketAppEntities();

        public ActionResult Index()
        {
            var employees = db.Employees.Include(e => e.AspNetUser).Include(e => e.Office);
            return View(employees.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        public ActionResult Create()
        {
            ViewBag.userCode = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.officeCode = new SelectList(db.Offices, "officeCode", "officeAddress");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "employeeCode,userCode,officeCode,employeeName,employeePhone,employeeAddress,createdDate")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.userCode = new SelectList(db.AspNetUsers, "Id", "Email", employee.userCode);
            ViewBag.officeCode = new SelectList(db.Offices, "officeCode", "officeAddress", employee.officeCode);
            return View(employee);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.userCode = new SelectList(db.AspNetUsers, "Id", "Email", employee.userCode);
            ViewBag.officeCode = new SelectList(db.Offices, "officeCode", "officeAddress", employee.officeCode);
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "employeeCode,userCode,officeCode,employeeName,employeePhone,employeeAddress,createdDate")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.userCode = new SelectList(db.AspNetUsers, "Id", "Email", employee.userCode);
            ViewBag.officeCode = new SelectList(db.Offices, "officeCode", "officeAddress", employee.officeCode);
            return View(employee);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
