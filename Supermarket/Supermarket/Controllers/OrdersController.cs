﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Supermarket.Models;

namespace Supermarket.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private SupermarketAppEntities db = new SupermarketAppEntities();

        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.Customer).Include(o => o.Employee).Include(o => o.Status).Include(o => o.Payment);
            return View(orders.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.customerCode = new SelectList(db.Customers, "customerCode", "customerName", order.customerCode);
            ViewBag.employeeCode = new SelectList(db.Employees, "employeeCode", "employeeName", order.employeeCode);
            ViewBag.statusCode = new SelectList(db.Status, "statusCode", "statusName", order.statusCode);
            ViewBag.paymentCode = new SelectList(db.Payments, "paymentCode", "paymentName", order.paymentCode);
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "orderCode,employeeCode,customerCode,statusCode,paymentCode,orderDate,shippedDate,comments,createdDate")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.customerCode = new SelectList(db.Customers, "customerCode", "customerName", order.customerCode);
            ViewBag.employeeCode = new SelectList(db.Employees, "employeeCode", "employeeName", order.employeeCode);
            ViewBag.statusCode = new SelectList(db.Status, "statusCode", "statusName", order.statusCode);
            ViewBag.paymentCode = new SelectList(db.Payments, "paymentCode", "paymentName", order.paymentCode);
            return View(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
