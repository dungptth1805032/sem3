﻿using Supermarket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Supermarket.Controllers
{
    public class LineChartProduct
    {
        public string productName { get; set; }
        public List<string> soldProduct = new List<string>();
        public List<string> soldMonth = new List<string>();
    }

    [Authorize]
    public class HomeController : Controller
    {
        private SupermarketAppEntities db = new SupermarketAppEntities();
        public ActionResult Index()
        {
            ViewBag.CustomersNumber = db.Customers.ToList().Count;
            ViewBag.EmployeesNumber = db.Employees.ToList().Count;
            ViewBag.OrdersNumber = db.Orders.ToList().Count;
            ViewBag.ProductsNumber = db.Products.ToList().Count;
            return View();
        }

        public ActionResult PieChart()
        {
            List<Models.Product> products = db.Products.ToList();
            List<string> name = new List<string>();
            List<string> qtySold = new List<string>();

            foreach (Models.Product product in products)
            {
                name.Add(product.productName);
                int sold = 0;
                foreach (Models.OrderDeatail od in product.OrderDeatails)
                {
                    if (od.productCode == product.productCode)
                    {
                        sold += od.quantityOrdered;
                    }
                }
                qtySold.Add(sold.ToString());
            }

            ViewData["Name"] = name;
            ViewData["QtySold"] = qtySold;
            return View();
        }

        public ActionResult ColumnChart()
        {
            List<Models.Product> products = db.Products.ToList();
            List<string> productsName = new List<string>();
            List<string> availableQty = new List<string>();
            List<string> qlisQty = new List<string>();

            foreach (Models.Product product in products)
            {
                productsName.Add(product.productName);
                availableQty.Add(product.productAvailable.ToString());
                qlisQty.Add(product.productLnStock.ToString());
            }

            ViewData["productsName"] = productsName;
            ViewData["availableQty"] = availableQty;
            ViewData["qlisQty"] = qlisQty;
            return View();
        }

        public ActionResult MonthlyChart()
        {
            // Group order by month
            var odGroupByMonth = db.OrderDeatails
                .GroupBy(m => new {
                    Month = m.createdDate.Value.Month
                })
                .OrderByDescending(m => m.Key)
                .Take(5).ToList();
            odGroupByMonth.Reverse();

            List<LineChartProduct> lcps = new List<LineChartProduct>();
            // Get product
            foreach (Product product in db.Products.ToList())
            {
                LineChartProduct lcp = new LineChartProduct();
                lcp.productName = product.productName;
                foreach(var odByMonth in odGroupByMonth)
                {
                    int soldProduct = 0;
                    foreach(var od in odByMonth)
                    {
                        if (od.productCode == product.productCode)
                        {
                            soldProduct += od.quantityOrdered;
                        }

                        if (
                            od.createdDate != null 
                            && !lcp.soldMonth.Contains(od.createdDate.Value.Month.ToString())
                        )
                        {
                            lcp.soldMonth.Add(od.createdDate.Value.Month.ToString());
                        }
                    }

                    lcp.soldProduct.Add(soldProduct.ToString());
                }
                lcps.Add(lcp);
            }

            ViewData["lcps"] = lcps;
            return View();
        }
    }
}