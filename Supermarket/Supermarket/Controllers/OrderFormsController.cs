﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Supermarket.Models;

namespace Supermarket.Controllers
{
    [Authorize]
    public class OrderFormsController : Controller
    {
        private SupermarketAppEntities db = new SupermarketAppEntities();

        public ActionResult Create()
        {
            ViewBag.productCode = new SelectList(db.Products, "productCode", "productName");
            ViewBag.employeeCode = new SelectList(db.Employees, "employeeCode", "employeeName");
            ViewBag.statusCode = new SelectList(db.Status, "statusCode", "statusName");
            ViewBag.paymentCode = new SelectList(db.Payments, "paymentCode", "paymentName");
            ViewBag.orderFormCode = 0; // Mark for the first time access
            ViewBag.productsList = new List<OrderFormProductList>();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "orderFormCode,customerName,customerPhone,customerAddress,productCode,productQuantity,employeeCode,orderDate,shippedDate,comments,createdDate,statusCode, paymentCode")] OrderForm orderForm)
        {
            // State 1: Update or create Order Form
            if (orderForm.orderFormCode == 0)
            {
                db.OrderForms.Add(orderForm);
                db.SaveChanges();
            }
            else
            {
                db.Entry(orderForm).State = EntityState.Modified;
                db.SaveChanges();
            }

            if (orderForm.statusCode > 1)
            {
                int orderCode = AsyncFormOrder(orderForm);
                return RedirectToAction("Details", "Orders", new { id = orderCode });
            }
            else
            {
                // Sate 2: Create Order Form Product
                OrderFormProductList OFPL = new OrderFormProductList();

                OFPL.productCode = orderForm.productCode;
                OFPL.orderFormCode = orderForm.orderFormCode;
                OFPL.productQuantity = orderForm.productQuantity;

                Product pro = db.Products.Find(orderForm.productCode);
                OFPL.productName = pro.productName;
                OFPL.productPrice = pro.productPrice;

                db.OrderFormProductLists.Add(OFPL);
                db.SaveChanges();
            }

            ViewBag.productCode = new SelectList(db.Products, "productCode", "productName");
            ViewBag.employeeCode = new SelectList(db.Employees, "employeeCode", "employeeName");
            ViewBag.statusCode = new SelectList(db.Status, "statusCode", "statusName");
            ViewBag.orderFormCode = orderForm.orderFormCode;
            ViewBag.paymentCode = new SelectList(db.Payments, "paymentCode", "paymentName");

            ViewBag.productsList = db.OrderFormProductLists
                .Where(ofpl => ofpl.orderFormCode == orderForm.orderFormCode).ToList();

            return PartialView("Create");
        }

        public int AsyncFormOrder(OrderForm orderForm)
        {
            int customerCode = UpdateOrCreateCustomer(orderForm);

            // Create order
            Order order = new Order();
            order.employeeCode = orderForm.employeeCode;
            order.customerCode = customerCode;
            order.statusCode = orderForm.statusCode;
            order.paymentCode = orderForm.paymentCode;

            order.orderDate = orderForm.orderDate != null ? orderForm.orderDate : DateTime.Now;
            order.shippedDate = orderForm.shippedDate != null ? orderForm.shippedDate : DateTime.Now;
            order.comments = orderForm.comments;
            order.createdDate = orderForm.createdDate != null ? orderForm.createdDate : DateTime.Now;
            db.Orders.Add(order);
            db.SaveChanges(); //From now orderCode will be accessable

            // Create order deatails
            List<OrderFormProductList> OFPLS = db.OrderFormProductLists
                .Where(o => o.orderFormCode == orderForm.orderFormCode).ToList();
            foreach (OrderFormProductList ofpl in OFPLS)
            {
                OrderDeatail od = new OrderDeatail();
                od.orderCode = order.orderCode;
                od.productCode = ofpl.productCode;
                od.quantityOrdered = ofpl.productQuantity;
                od.createdDate = orderForm.createdDate != null ? orderForm.createdDate : DateTime.Now;
                db.OrderDeatails.Add(od);
                db.SaveChanges();

                // Update product
                Product pro = db.Products.Find(ofpl.productCode);
                pro.lastSold = orderForm.createdDate != null ? orderForm.createdDate : DateTime.Now;
                pro.productAvailable -= ofpl.productQuantity;
                db.Entry(pro).State = EntityState.Modified;
                db.SaveChanges();
            }
            return order.orderCode;
        }

        public int UpdateOrCreateCustomer(OrderForm orderForm)
        {
            List<Customer> customers = db.Customers
                .Where(cus => cus.customerPhone == orderForm.customerPhone).ToList();
            int customerCode;
            if (customers.Count > 0)
            {
                customers[0].customerName = orderForm.customerName;
                customers[0].customerPhone = orderForm.customerPhone;
                customers[0].customerAddress = orderForm.customerAddress;
                db.Entry(customers[0]).State = EntityState.Modified;
                db.SaveChanges();
                customerCode = customers[0].customerCode;
            }
            else
            {
                Customer customer = new Customer();
                customer.customerName = orderForm.customerName;
                customer.customerPhone = orderForm.customerPhone;
                customer.customerAddress = orderForm.customerAddress;
                customer.createdDate = orderForm.createdDate != null ? orderForm.createdDate : DateTime.Now;
                db.Customers.Add(customer);
                db.SaveChanges();
                customerCode = customer.customerCode;
            }

            return customerCode;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
