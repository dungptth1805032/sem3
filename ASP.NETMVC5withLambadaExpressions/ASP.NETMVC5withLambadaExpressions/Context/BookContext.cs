﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASP.NETMVC5withLambadaExpressions.Context
{
    public class BookContext : DbContext
    {
        public DbSet<Models.Book> Books { get; set; }
    }
}